library PeakMeter;
{$R *.dres}

uses
  apiPlugin,
  Displays in 'Source\Common\Displays.pas',
  GlobalData in 'Source\Common\GlobalData.pas',
  AIMPOptionsFramePeakMeter in 'Source\Common\AIMPOptionsFramePeakMeter.pas',
  OSD in 'Source\Main\OSD.pas' {PeakMeterOSD} ,
  Plugin in 'Source\Main\Plugin.pas',
  Visualization in 'Source\DataModel\Visualization.pas',
  Models in 'Source\DataModel\Models.pas',
  Setting in 'Source\DataModel\Setting.pas',
  LayeredWindow in 'Source\Common\LayeredWindow.pas',
  FloatSpinEdit in 'Source\Components\FloatSpinEdit.pas',
  AIMPOptionsBaseFrame in 'Source\Helpers\AIMPOptionsBaseFrame.pas';

{$R *.res}

function AIMPPluginGetHeader(out Header: IAIMPPlugin): HRESULT; stdcall;
begin
  try
    Header := TMeterPlugin.Create;
    Result := S_OK;
  except
    Result := E_UNEXPECTED;
  end;
end;

exports AIMPPluginGetHeader;

begin

end.
