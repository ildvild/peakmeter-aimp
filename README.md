# Description

 A plugin for the AIMP player. 

 It displays peak level of the signal. The plugin idea is taken from [Volume2](https://irzyxa.wordpress.com/)

 See also: [Peak Meter Topic](http://www.aimp.ru/forum/index.php?topic=45615.0)

# Needed for build
* [SDK for AIMP](http://aimp.ru/index.php?do=download&cat=sdk)
* [gdiplus](http://www.bilsen.com/gdiplus/index.shtml)
* Install FloatSpinEdit component (in source code)

# Contacts
* e-mail: ildvild@gmail.com
* AIMP forum: http://www.aimp.ru/forum/index.php?topic=45615.0