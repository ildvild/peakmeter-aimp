unit FloatSpinEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Spin, ComCtrls;

type
  { TFloatSpinEdit }

  TFloatSpinEdit = class(TCustomEdit)
  private
    // FButton: TSpinButton;
    FButton: TUpDown;
    FEditorEnabled: Boolean;
    FIncrement: Double;
    FMaxValue: Double;
    FMinValue: Double;
    FOnlyPositive: Boolean; // �������� ������ ������ ����
    FPrecision: byte;
    function CheckValue(NewValue: Double): Double;
    procedure CMEnter(var Message: TCMGotFocus);
    message CM_ENTER;
    procedure CMExit(var Message: TCMExit);
    message CM_EXIT;
    function GetMinHeight: Integer;
    function GetValue: Double;
    procedure SetEditRect;
    procedure SetPrecision(NewValue: byte);
    procedure SetValue(NewValue: Double);
    procedure WMCut(var Message: TWMCut);
    message WM_CUT;
    procedure WMPaste(var Message: TWMPaste);
    message WM_PASTE;
    procedure WMSize(var Message: TWMSize);
    message WM_SIZE;
  protected
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    procedure DownClick(Sender: TObject); virtual;
    function IsValidChar(Key: Char): Boolean; virtual;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure UpClick(Sender: TObject); virtual;
    procedure UpDownChangingEx(Sender: TObject; var AllowChange: Boolean;
      NewValue: Smallint; Direction: TUpDownDirection);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
    property Button: TUpDown read FButton;
  published
    property Anchors;
    property AutoSelect;
    property AutoSize;
    property Color;
    property Constraints;
    property Ctl3D;
    property DragCursor;
    property DragMode;
    property EditorEnabled
      : Boolean read FEditorEnabled write FEditorEnabled default True;
    property Enabled;
    property Font;
    property Increment: Double read FIncrement write FIncrement;
    property MaxLength;
    property MaxValue: Double read FMaxValue write FMaxValue;
    property MinValue: Double read FMinValue write FMinValue;
    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnlyPositive
      : Boolean read FOnlyPositive write FOnlyPositive default False;
    // �������� ������ ������ ����
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDrag;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property Precision: byte read FPrecision write SetPrecision;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Value: Double read GetValue write SetValue;
    property Visible;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Samples', [TFloatSpinEdit]);
end;

{ TFloatSpinEdit }

constructor TFloatSpinEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  // FButton := TSpinButton.Create (Self);
  FButton := TUpDown.Create(Self);
  FButton.Width := 15;
  FButton.Height := 20;
  // FButton.Height := 17;
  FButton.Visible := True;
  FButton.Parent := Self;
  FButton.Min := -32767;
  FButton.Max := 32767;
  FButton.OnChangingEx := UpDownChangingEx;
  // FButton.FocusControl := Self;
  // FButton.OnUpClick := UpClick;
  // FButton.OnDownClick := DownClick;
  Text := FloatToStrF(Value, ffGeneral, 8, Precision);
  ControlStyle := ControlStyle - [csSetCaption];
  FIncrement := 1;
  FPrecision := 1;
  FEditorEnabled := True;
  FOnlyPositive := False;
end;

destructor TFloatSpinEdit.Destroy;
begin
  FButton := nil;
  inherited Destroy;
end;

function TFloatSpinEdit.CheckValue(NewValue: Double): Double;
begin
  Result := NewValue;
  if (FMaxValue <> FMinValue) then
  begin
    if NewValue < FMinValue then
      Result := FMinValue
    else if NewValue > FMaxValue then
      Result := FMaxValue;
  end;
end;

procedure TFloatSpinEdit.CMEnter(var Message: TCMGotFocus);
begin
  if AutoSelect and not(csLButtonDown in ControlState) then
    SelectAll;
  inherited;
end;

procedure TFloatSpinEdit.CMExit(var Message: TCMExit);
begin
  inherited;
  if CheckValue(Value) <> Value then
    SetValue(Value);
end;

procedure TFloatSpinEdit.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  { Params.Style := Params.Style and not WS_BORDER; }
  Params.Style := Params.Style or ES_MULTILINE or WS_CLIPCHILDREN;
end;

procedure TFloatSpinEdit.CreateWnd;
begin
  inherited CreateWnd;
  SetEditRect;
end;

procedure TFloatSpinEdit.DownClick(Sender: TObject);
begin
  if ReadOnly then
    MessageBeep(0)
  else
    Value := Value - FIncrement;
end;

procedure TFloatSpinEdit.GetChildren(Proc: TGetChildProc; Root: TComponent);
begin
end;

function TFloatSpinEdit.GetMinHeight: Integer;
var
  DC: HDC;
  I: Integer;
  Metrics: TTextMetric;
  SaveFont: HFont;
  SysMetrics: TTextMetric;
begin
  DC := GetDC(0);
  GetTextMetrics(DC, SysMetrics);
  SaveFont := SelectObject(DC, Font.Handle);
  GetTextMetrics(DC, Metrics);
  SelectObject(DC, SaveFont);
  ReleaseDC(0, DC);
  I := SysMetrics.tmHeight;
  if I > Metrics.tmHeight then
    I := Metrics.tmHeight;
  Result := Metrics.tmHeight + I div 4 + GetSystemMetrics(SM_CYBORDER) * 4 + 2;
end;

function TFloatSpinEdit.GetValue: Double;
begin

  if not TryStrToFloat(Text, Result) then
    Result := FMinValue;
  { try
    Result := StrToFloat (Text);
    except
    Result := FMinValue;
    end; }
end;

function TFloatSpinEdit.IsValidChar(Key: Char): Boolean;
begin
  Result := (CharInSet(Key, [DecimalSeparator, '+', '-', '0' .. '9'])) or
    ((Key < #32) and (Key <> Chr(VK_RETURN)));
  if not FEditorEnabled and Result and ((Key >= #32) or (Key = Char(VK_BACK))
      or (Key = Char(VK_DELETE))) then
    Result := False;
end;

procedure TFloatSpinEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited KeyDown(Key, Shift);
  if Key = VK_UP then
    UpClick(Self)
  else if Key = VK_DOWN then
    DownClick(Self);
end;

procedure TFloatSpinEdit.KeyPress(var Key: Char);
begin
  inherited KeyPress(Key);
  if not IsValidChar(Key) then
  begin
    Key := #0;
    MessageBeep(0)
  end;
end;

procedure TFloatSpinEdit.SetEditRect;
var
  Loc: TRect;
begin
  SendMessage(Handle, EM_GETRECT, 0, LongInt(@Loc));
  Loc.Bottom := ClientHeight + 1; { +1 is workaround for windows paint bug }
  Loc.Right := ClientWidth - FButton.Width - 2;
  Loc.Top := 0;
  Loc.Left := 0;
  SendMessage(Handle, EM_SETRECTNP, 0, LongInt(@Loc));
  SendMessage(Handle, EM_GETRECT, 0, LongInt(@Loc)); { debug }
end;

procedure TFloatSpinEdit.SetPrecision(NewValue: byte);
begin
  FPrecision := NewValue;
  SetValue(GetValue);
end;

procedure TFloatSpinEdit.SetValue(NewValue: Double);
begin
  if (OnlyPositive and (NewValue < 0)) then
    NewValue := 0;

  Text := FloatToStrF(CheckValue(NewValue), ffFixed, FPrecision + 8,
    FPrecision);
end;

procedure TFloatSpinEdit.UpClick(Sender: TObject);
begin
  if ReadOnly then
    MessageBeep(0)
  else
    Value := Value + FIncrement;
end;

procedure TFloatSpinEdit.UpDownChangingEx
  (Sender: TObject; var AllowChange: Boolean; NewValue: Smallint;
  Direction: TUpDownDirection);
begin
  case Direction of
    updUp:
      begin
        UpClick(Sender);
      end;
    updDown:
      begin
        DownClick(Sender);
      end;
  end;
end;

procedure TFloatSpinEdit.WMCut(var Message: TWMPaste);
begin
  if not FEditorEnabled or ReadOnly then
    Exit;
  inherited;
end;

procedure TFloatSpinEdit.WMPaste(var Message: TWMPaste);
begin
  if not FEditorEnabled or ReadOnly then
    Exit;
  inherited;
end;

procedure TFloatSpinEdit.WMSize(var Message: TWMSize);
var
  MinHeight: Integer;
begin
  inherited;
  MinHeight := GetMinHeight;
  { text edit bug: if size to less than minheight, then edit ctrl does
    not display the text }
  if Height < MinHeight then
    Height := MinHeight
  else if FButton <> nil then
  begin
    if NewStyleControls and Ctl3D then
      FButton.SetBounds(Width - FButton.Width - 5, 0, FButton.Width, Height - 5)
    else
      FButton.SetBounds(Width - FButton.Width, 1, FButton.Width, Height - 3);
    SetEditRect;
  end;
end;

end.
