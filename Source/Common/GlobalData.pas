unit GlobalData;

interface

const
  SOptionFrameName = 'Peak Meter';

  SInfoName = 'PeakMeter';
  SAuthor = 'ildvild';
  SDescription = '»ндикатор пикового уровн€';
  SFullDescription = '';
  // 'ѕоказать\скрыть индикатор можно через меню утилиты, либо с помощью гор€чих клавиш, либо через меню самого индикатора.';
  SSkinFolderName = 'Meter Skins';
  SVersion = '1.2.3';
  SForumUrl = 'http://www.aimp.ru/forum/index.php?topic=45615.0';

function GetPluginPath: string;

implementation

uses
  apiObjects, apiCore, apiWrappers;

function GetPluginPath: string;
var
  sPath: IAIMPString;
begin
  Result := '';
  CoreIntf.GetPath(AIMP_CORE_PATH_PLUGINS, sPath);
  try
    Result := sPath.GetData;
  finally
    sPath := nil;
  end;
end;

end.
