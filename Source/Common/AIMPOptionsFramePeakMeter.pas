unit AIMPOptionsFramePeakMeter;

{ TODO -oOwner -cGeneral : �������� ����� }
interface

uses
  Windows, Classes, Graphics, Controls, Forms, StdCtrls,
  AIMPOptionsBaseFrame, Menus, FloatSpinEdit, Spin,
  apiOptions, apiObjects, Plugin;

type
  TAIMPOptionsFramePeakMeter = class(TAIMPOptionsBaseFrame)
    cbbMonitorIndex: TComboBox;
    chkInScreenCenter: TCheckBox;
    fsePositionX: TFloatSpinEdit;
    fsePositionY: TFloatSpinEdit;
    lblMonitorIndex: TLabel;
    lblTransparent: TLabel;
    lblPositionX: TLabel;
    lblPositionY: TLabel;
    lblTransparentPersent: TLabel;
    lblPositionXPersent: TLabel;
    lblPositionYPersent: TLabel;
    lstPeakMeterSkins: TListBox;
    seMeterTransparent: TSpinEdit;
    pmDeleteSkin: TPopupMenu;
    mniDeleteSkin: TMenuItem;
    chkHideWhenStopped: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure cbbMonitorIndexChange(Sender: TObject);
    procedure chkInScreenCenterClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lstPeakMeterSkinsDblClick(Sender: TObject);
    procedure lstPeakMeterSkinsDrawItem(Control: TWinControl; Index: Integer;
      Rect: TRect; State: TOwnerDrawState);
    procedure seMeterTransparentChange(Sender: TObject);
    procedure fsePositionXChange(Sender: TObject);
    procedure fsePositionYChange(Sender: TObject);
    procedure mniDeleteSkinClick(Sender: TObject);
    procedure pmDeleteSkinPopup(Sender: TObject);
    procedure chkHideWhenStoppedClick(Sender: TObject);
  private
    procedure SetActivePositionControl(isActive: Boolean);
    procedure SetControlPosition(R: TRect);
    function GetSkinIndexByName(const AName: string): Integer;
  end;

type
  TOptionFrame = class(TInterfacedObject, IAIMPOptionsDialogFrame)
  private
    FPlugin: TMeterPlugin;
    FFrame: TAIMPOptionsFramePeakMeter;
    procedure DoModified;
  protected
    function CreateFrame(ParentWnd: HWND): HWND; stdcall;
    procedure DestroyFrame; stdcall;
    function GetName(out S: IAIMPString): HRESULT; stdcall;
    procedure Notification(ID: Integer); stdcall;
    procedure UpdateLanguage;

  public
    property Frame: TAIMPOptionsFramePeakMeter read FFrame write FFrame;
    property Plugin: TMeterPlugin read FPlugin write FPlugin;
    procedure DoShow(ForceShow: LongBool = True);
    procedure UpdateFromSetting; // ��������� GUI � �����. � �����������
    procedure UpdateToSetting; // ��������� ��������� � ����� � GUI
  end;

var
  OptionFrame: TOptionFrame;

implementation

uses
  Displays, SysUtils, apiWrappers, OSD, Setting,
  GlobalData, shellapi, pngimage, Models;

{$R *.dfm}

function DelDir(const dir: string): Boolean;
var
  fos: TSHFileOpStruct;
begin
  ZeroMemory(@fos, SizeOf(fos));
  with fos do
  begin
    wFunc := FO_DELETE;
    fFlags := FOF_SILENT or FOF_NOCONFIRMATION;
    pFrom := PChar(dir + #0);
  end;
  Result := (0 = ShFileOperation(fos));
end;

procedure TAIMPOptionsFramePeakMeter.FormCreate(Sender: TObject);
begin
  lstPeakMeterSkins.DoubleBuffered := True;
  ForumUrl := SForumUrl;
end;

procedure TAIMPOptionsFramePeakMeter.cbbMonitorIndexChange(Sender: TObject);
begin
  OptionFrame.DoModified;
end;

procedure TAIMPOptionsFramePeakMeter.chkHideWhenStoppedClick(Sender: TObject);
begin
  OptionFrame.DoModified;
end;

procedure TAIMPOptionsFramePeakMeter.chkInScreenCenterClick(Sender: TObject);
begin
  SetActivePositionControl(not chkInScreenCenter.Checked);
  OptionFrame.DoModified;
end;

procedure TAIMPOptionsFramePeakMeter.FormShow(Sender: TObject);
var
  i: Integer;
begin
  cbbMonitorIndex.Clear;
  for i := Low(MonList) to High(MonList) do
    cbbMonitorIndex.Items.Add(MonList[i].szDevice);

  if cbbMonitorIndex.Items.Count > 0 then
  begin
    cbbMonitorIndex.ItemIndex := 0;
    cbbMonitorIndex.Enabled := True;
  end
  else
    cbbMonitorIndex.Enabled := false;

end;

function TAIMPOptionsFramePeakMeter.GetSkinIndexByName
  (const AName: string): Integer;
var
  i: Integer;
begin
  Result := -1;
  if not Assigned(lstPeakMeterSkins) then
    exit;
  for i := 0 to lstPeakMeterSkins.Count - 1 do
  begin
    if (lstPeakMeterSkins.Items.Objects[i] as TMeterSkin).Name = AName then
    begin
      Result := i;
      Break;
    end;
  end;
end;

procedure TAIMPOptionsFramePeakMeter.lstPeakMeterSkinsDblClick(Sender: TObject);
begin
  if (Sender as TListBox).ItemIndex >= 0 then
  begin
    CurrentMeterSkin.GetData((Sender as TListBox).Items.Objects
      [(Sender as TListBox).ItemIndex] as TMeterSkin);

    PeakMeterOSD.Width := CurrentMeterSkin.BackPng.Width;
    PeakMeterOSD.Height := CurrentMeterSkin.BackPng.Height;
    PeakMeterOSD.LayeredWindow.Surface.SetSize(CurrentMeterSkin.BackPng.Width,
      CurrentMeterSkin.BackPng.Height);
    PeakMeterOSD.InScreenCenter := PluginSetting.InScreenCenter; //

    PluginSetting.SkinName := CurrentMeterSkin.Skin.Name;

    lstPeakMeterSkins.Repaint;
    // OptionFrame.DoModified;
  end;

end;

procedure TAIMPOptionsFramePeakMeter.lstPeakMeterSkinsDrawItem
  (Control: TWinControl; Index: Integer; Rect: TRect; State: TOwnerDrawState);
var
  List: TListBox;
  tmpImage: TPngimage;
  tmpSkinMeter: TMeterSkin;
begin
  // 390px �� ��������

  List := Control as TListBox;
  tmpSkinMeter := (List.Items.Objects[Index] as TMeterSkin);

  tmpImage := TPngimage.Create;
  try
    tmpImage.LoadFromFile(tmpSkinMeter.Files.PreviewPng);
    with List.Canvas, Rect do
    begin
      Font.Color := clGray;

      if odSelected in State then
        Brush.Color := RGB(255, 213, 159)
      else
        Brush.Color := clWindow;

      FillRect(Rect);

      if odFocused in State then
        Windows.DrawFocusRect(List.Canvas.Handle, Rect);

      // Preview
      Draw((Left + 4 + (94 - tmpImage.Width) div 2),
        Top + (abs(Rect.Top - Rect.Bottom) - tmpImage.Height) div 2, tmpImage);

      // Info
      Font.Color := clBlack;
      if Index = GetSkinIndexByName(CurrentMeterSkin.Skin.Name) then
        Font.Style := Font.Style + [fsBold];
      TextOut(Left + 110, Top + 5, List.Items[Index]);
      Font.Style := Font.Style - [fsBold];
      Font.Color := clGray;
      TextOut(Left + 110, Top + 20, tmpSkinMeter.Info.Author);
      TextOut(Left + 110, Top + 35, tmpSkinMeter.Info.Website);

    end;
  finally
    tmpImage.free;
  end;
end;

procedure TAIMPOptionsFramePeakMeter.mniDeleteSkinClick(Sender: TObject);
var
  dir: string;
begin
  if lstPeakMeterSkins.ItemIndex <> -1 then
  begin
    dir := GetPluginPath + SInfoName + '\' + SSkinFolderName + '\' +
      (lstPeakMeterSkins.Items.Objects[lstPeakMeterSkins.ItemIndex]
      as TMeterSkin).Name;

    if DelDir(dir) then
    begin
      lstPeakMeterSkins.Items.Objects[lstPeakMeterSkins.ItemIndex].free;
      lstPeakMeterSkins.Items.Delete(lstPeakMeterSkins.ItemIndex);
    end;
  end;
end;

procedure TAIMPOptionsFramePeakMeter.pmDeleteSkinPopup(Sender: TObject);
begin
  if lstPeakMeterSkins.ItemIndex <> -1 then
    mniDeleteSkin.Enabled := True
  else
    mniDeleteSkin.Enabled := false
end;

procedure TAIMPOptionsFramePeakMeter.seMeterTransparentChange(Sender: TObject);
begin
  OptionFrame.DoModified;
end;

procedure TAIMPOptionsFramePeakMeter.SetActivePositionControl
  (isActive: Boolean);
begin
  lblPositionX.Enabled := isActive;
  lblPositionY.Enabled := isActive;
  lblPositionXPersent.Enabled := isActive;
  lblPositionYPersent.Enabled := isActive;
  fsePositionX.Enabled := isActive;
  fsePositionY.Enabled := isActive;
end;

procedure TAIMPOptionsFramePeakMeter.SetControlPosition(R: TRect);
const
  LeftMargin = 12;
  TopMargin = 24;
var
  fw: Integer;
begin
  fw := R.Bottom - R.Top;

  lstPeakMeterSkins.Height := 220;

  lblMonitorIndex.Top := lstPeakMeterSkins.Top + lstPeakMeterSkins.Height + 10;
  lblMonitorIndex.Left := LeftMargin;
  lblTransparent.Top := lblMonitorIndex.Top + TopMargin;
  lblTransparent.Left := LeftMargin;
  chkInScreenCenter.Top := lblTransparent.Top + TopMargin;
  chkInScreenCenter.Left := LeftMargin;
  chkInScreenCenter.Width := lstPeakMeterSkins.Width - LeftMargin + 10;
  lblPositionX.Top := chkInScreenCenter.Top + TopMargin;
  lblPositionX.Left := LeftMargin;
  lblPositionY.Top := lblPositionX.Top + TopMargin;
  lblPositionY.Left := LeftMargin;

  cbbMonitorIndex.Top := lblMonitorIndex.Top -
    abs(cbbMonitorIndex.Height - lblMonitorIndex.Height) div 2;
  cbbMonitorIndex.Left := fw div 2 + LeftMargin;

  seMeterTransparent.Top := lblTransparent.Top -
    abs(seMeterTransparent.Height - lblTransparent.Height) div 2;
  seMeterTransparent.Left := fw div 2 + LeftMargin;
  lblTransparentPersent.Top := lblTransparent.Top;
  lblTransparentPersent.Left := seMeterTransparent.Left +
    seMeterTransparent.Width + 5;

  fsePositionX.Top := lblPositionX.Top -
    abs(fsePositionX.Height - lblPositionX.Height) div 2;
  fsePositionX.Left := fw div 2 + LeftMargin;
  lblPositionXPersent.Top := lblPositionX.Top;
  lblPositionXPersent.Left := lblTransparentPersent.Left;

  fsePositionY.Top := lblPositionY.Top -
    abs(fsePositionY.Height - lblPositionY.Height) div 2;
  fsePositionY.Left := fw div 2 + LeftMargin;
  lblPositionYPersent.Top := lblPositionY.Top;
  lblPositionYPersent.Left := lblTransparentPersent.Left;

  chkHideWhenStopped.Top := lblPositionYPersent.Top + TopMargin;
  chkHideWhenStopped.Left := LeftMargin;
  chkHideWhenStopped.Width := chkInScreenCenter.Width;

  lblVersion.Left := Width - lblVersion.Width - 10;
  lblVersion.Top := Height - lblVersion.Height - 10;

end;

procedure TAIMPOptionsFramePeakMeter.fsePositionXChange(Sender: TObject);
begin
  OptionFrame.DoModified;
end;

procedure TAIMPOptionsFramePeakMeter.fsePositionYChange(Sender: TObject);
begin
  OptionFrame.DoModified;
end;

{ TOptionFrame }

function TOptionFrame.CreateFrame(ParentWnd: HWND): HWND;
var
  i: Integer;
  R: TRect;
begin
  FreeAndNil(FFrame);
  FFrame := TAIMPOptionsFramePeakMeter.CreateParented(ParentWnd);
  GetWindowRect(ParentWnd, R);
  OffsetRect(R, -R.Left, -R.Top);
  FFrame.BoundsRect := R;
  FFrame.SetControlPosition(R);
  //
  if Assigned(FPlugin) then
    if Assigned(FFrame) then
      for i := 0 to FPlugin.MeterSkinList.Count - 1 do
        FFrame.lstPeakMeterSkins.Items.AddObject(FPlugin.MeterSkinList[i].Name,
          FPlugin.MeterSkinList[i]);
  //
  Result := FFrame.Handle;
end;

procedure TOptionFrame.DestroyFrame;
begin
  FreeAndNil(FFrame);
end;

procedure TOptionFrame.DoModified;
var
  AIMPServiceOptionsDialog: IAIMPServiceOptionsDialog;
begin
  CoreGetService(IAIMPServiceOptionsDialog, AIMPServiceOptionsDialog);
  try
    AIMPServiceOptionsDialog.FrameModified(Self);
  finally
    AIMPServiceOptionsDialog := nil;
  end;

end;

procedure TOptionFrame.DoShow(ForceShow: LongBool = True);
var
  AIMPServiceOptionsDialog: IAIMPServiceOptionsDialog;
begin
  CoreGetService(IAIMPServiceOptionsDialog, AIMPServiceOptionsDialog);
  try
    AIMPServiceOptionsDialog.FrameShow(Self, ForceShow);
  finally
    AIMPServiceOptionsDialog := nil;
  end;

end;

function TOptionFrame.GetName(out S: IAIMPString): HRESULT;
begin
  if LangLoadString('PeakMeter\Name', S) <> S_OK then
    S := MakeString(SOptionFrameName);
  Result := S_OK;
end;

procedure TOptionFrame.Notification(ID: Integer);
begin

  case ID of
    AIMP_SERVICE_OPTIONSDIALOG_NOTIFICATION_LOAD:
      begin
        // PluginSetting.LoadSetting;
        UpdateFromSetting;
      end;

    AIMP_SERVICE_OPTIONSDIALOG_NOTIFICATION_LOCALIZATION:
      begin
        UpdateLanguage;
      end;

    AIMP_SERVICE_OPTIONSDIALOG_NOTIFICATION_SAVE:
      begin
        UpdateToSetting;
        //
        PeakMeterOSD.InScreenCenter := PluginSetting.InScreenCenter;
        PeakMeterOSD.MeterTransparent := PluginSetting.MeterTransparent;
        //
        PluginSetting.SaveSetting;
      end;
  end;
end;

procedure TOptionFrame.UpdateFromSetting;
begin
  if Assigned(FFrame) then
  begin
    FFrame.lstPeakMeterSkins.ItemIndex := FFrame.GetSkinIndexByName
      (PluginSetting.SkinName);

    FFrame.chkInScreenCenter.Checked := PluginSetting.InScreenCenter;
    FFrame.fsePositionX.Value := PluginSetting.PositionX;
    FFrame.fsePositionY.Value := PluginSetting.PositionY;
    FFrame.seMeterTransparent.Value := PluginSetting.MeterTransparent;
    FFrame.cbbMonitorIndex.ItemIndex := PluginSetting.MonitorIndex;
    FFrame.chkHideWhenStopped.Checked := PluginSetting.HideWhenStopped;
  end;
end;

procedure TOptionFrame.UpdateToSetting;
begin
  PluginSetting.MonitorIndex := FFrame.cbbMonitorIndex.ItemIndex;
  PluginSetting.InScreenCenter := FFrame.chkInScreenCenter.Checked;
  PluginSetting.PositionX := FFrame.fsePositionX.Value;
  PluginSetting.PositionY := FFrame.fsePositionY.Value;
  PluginSetting.MeterTransparent := FFrame.seMeterTransparent.Value;
  PluginSetting.HideWhenStopped := FFrame.chkHideWhenStopped.Checked;
end;

procedure TOptionFrame.UpdateLanguage;
begin
  FFrame.PluginName := LangLoadString('PeakMeter\Name');
  FFrame.Font.Name := TFontName(LangLoadString('PeakMeter\FontName'));
  FFrame.Font.Size := StrToInt(LangLoadString('PeakMeter\FontSize'));

  FFrame.lblMonitorIndex.Caption :=
    LangLoadString('PeakMeter\ShowOnScreenNumber');
  FFrame.lblTransparent.Caption := LangLoadString('PeakMeter\Transparent');
  FFrame.chkInScreenCenter.Caption :=
    LangLoadString('PeakMeter\MeterInScreenCenter');
  FFrame.lblPositionX.Caption := LangLoadString('PeakMeter\OSDLeft');
  FFrame.lblPositionY.Caption := LangLoadString('PeakMeter\OSDTop');
  FFrame.lblTransparentPersent.Caption := LangLoadString('PeakMeter\Percent');
  FFrame.lblPositionXPersent.Caption := LangLoadString('PeakMeter\Percent');
  FFrame.lblPositionYPersent.Caption := LangLoadString('PeakMeter\Percent');
  FFrame.chkHideWhenStopped.Caption :=
    LangLoadString('PeakMeter\HideWhenStopped');
  FFrame.Version := '<a href="' + SForumUrl + '">' +
    LangLoadString('PeakMeter\Version') + ' ' + SVersion + '</a>';

  FFrame.SetControlPosition(FFrame.BoundsRect);
end;

end.
