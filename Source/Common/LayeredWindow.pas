unit LayeredWindow;

interface

uses
  Windows, SysUtils, Graphics, Classes, Controls;

const
  MaxAlpha = 255;

type
  TLayeredWindow = class(TGraphicControl)
  protected
    FAlpha: Byte;
    FSurface: TBitmap; // 32bit bitmap
    FOwner: TWinControl;
    BlendFunction: TBlendFunction;
    ZeroPosition: TPoint;
    FormSize: TSize;

    procedure SetSurface(const Value: TBitmap);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure UpdateLayer;
  published
    property Surface: TBitmap read FSurface write SetSurface;
    property Alpha: Byte read FAlpha write FAlpha default MaxAlpha;
  end;

implementation

{ TLayeredWindow }

constructor TLayeredWindow.Create(AOwner: TComponent);
begin
  inherited;

  FSurface := TBitmap.Create;
  FSurface.HandleType := bmDIB;
  FSurface.PixelFormat := pf32bit;

  FAlpha := MaxAlpha;

  FOwner := (AOwner as TWinControl);

  with BlendFunction do
  begin
    BlendOp := AC_SRC_OVER;
    BlendFlags := 0;
    SourceConstantAlpha := FAlpha;
    AlphaFormat := AC_SRC_ALPHA;
  end;

  ZeroPosition.x := 0;
  ZeroPosition.y := 0;

  { ������ �� ���� �������� ���� }
  SetWindowLong(FOwner.Handle, GWL_EXSTYLE, GetWindowLong(FOwner.Handle, GWL_EXSTYLE) +
    WS_EX_LAYERED);
end;

destructor TLayeredWindow.Destroy;
begin
  FreeAndNil(FSurface);
  inherited;
end;

procedure TLayeredWindow.SetSurface(const Value: TBitmap);
begin
  FSurface.Assign(Value);
end;

procedure TLayeredWindow.UpdateLayer;
begin
  BlendFunction.SourceConstantAlpha := FAlpha;

  FormSize.cx := FSurface.Width;
  FormSize.cy := FSurface.Height;

  UpdateLayeredWindow(FOwner.Handle, 0, nil, // if pallet is uncared, it's nil.
    @FormSize, // form size
    FSurface.Canvas.Handle, // DC of surface
    @ZeroPosition, // start position for surface
    0, @BlendFunction, ULW_ALPHA);
end;

end.
