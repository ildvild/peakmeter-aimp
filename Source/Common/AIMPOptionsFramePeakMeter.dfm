object AIMPOptionsFramePeakMeter: TAIMPOptionsFramePeakMeter
  Left = 0
  Top = 0
  BorderStyle = bsNone
  ClientHeight = 446
  ClientWidth = 489
  Color = 15790320
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Visible = True
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblMonitorIndex: TLabel
    Left = 10
    Top = 251
    Width = 115
    Height = 13
    Caption = #1042#1099#1074#1086#1076#1080#1090#1100' '#1085#1072' '#1084#1086#1085#1080#1090#1086#1088':'
  end
  object lblTransparent: TLabel
    Left = 10
    Top = 280
    Width = 75
    Height = 13
    Caption = #1055#1088#1086#1079#1088#1072#1095#1085#1086#1089#1090#1100':'
  end
  object lblPositionX: TLabel
    Left = 10
    Top = 342
    Width = 84
    Height = 13
    Caption = #1055#1086' '#1075#1086#1088#1080#1079#1086#1085#1090#1072#1083#1080':'
  end
  object lblPositionY: TLabel
    Left = 10
    Top = 371
    Width = 74
    Height = 13
    Caption = #1055#1086' '#1074#1077#1088#1090#1080#1082#1072#1083#1080':'
  end
  object lblTransparentPersent: TLabel
    Left = 311
    Top = 280
    Width = 12
    Height = 14
    Caption = '%'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblPositionXPersent: TLabel
    Left = 311
    Top = 342
    Width = 12
    Height = 14
    Caption = '%'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lblPositionYPersent: TLabel
    Left = 311
    Top = 371
    Width = 12
    Height = 14
    Caption = '%'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object lstPeakMeterSkins: TListBox
    AlignWithMargins = True
    Left = 10
    Top = 10
    Width = 469
    Height = 216
    Margins.Left = 10
    Margins.Top = 10
    Margins.Right = 10
    Margins.Bottom = 10
    Style = lbOwnerDrawFixed
    Align = alTop
    DoubleBuffered = True
    ItemHeight = 54
    ParentDoubleBuffered = False
    TabOrder = 0
    OnDblClick = lstPeakMeterSkinsDblClick
    OnDrawItem = lstPeakMeterSkinsDrawItem
  end
  object chkInScreenCenter: TCheckBox
    Left = 10
    Top = 309
    Width = 463
    Height = 17
    Caption = #1042' '#1094#1077#1085#1090#1088#1077' '#1101#1082#1088#1072#1085#1072
    TabOrder = 3
    OnClick = chkInScreenCenterClick
  end
  object cbbMonitorIndex: TComboBox
    Left = 234
    Top = 250
    Width = 142
    Height = 21
    AutoComplete = False
    Style = csDropDownList
    TabOrder = 1
    OnChange = cbbMonitorIndexChange
  end
  object seMeterTransparent: TSpinEdit
    Left = 234
    Top = 277
    Width = 67
    Height = 22
    MaxValue = 100
    MinValue = 0
    TabOrder = 2
    Value = 0
    OnChange = seMeterTransparentChange
  end
  object fsePositionX: TFloatSpinEdit
    Left = 234
    Top = 340
    Width = 67
    Height = 22
    Increment = 0.100000000000000000
    MaxValue = 100.000000000000000000
    OnChange = fsePositionXChange
    OnlyPositive = True
    Precision = 1
    TabOrder = 4
  end
  object fsePositionY: TFloatSpinEdit
    Left = 234
    Top = 368
    Width = 67
    Height = 22
    Increment = 0.100000000000000000
    MaxValue = 100.000000000000000000
    OnChange = fsePositionYChange
    OnlyPositive = True
    Precision = 1
    TabOrder = 5
  end
  object chkHideWhenStopped: TCheckBox
    Left = 10
    Top = 397
    Width = 463
    Height = 17
    Caption = #1057#1082#1088#1099#1074#1072#1090#1100' '#1087#1088#1080' '#1087#1088#1086#1089#1090#1086#1077
    TabOrder = 6
    OnClick = chkHideWhenStoppedClick
  end
  object pmDeleteSkin: TPopupMenu
    OnPopup = pmDeleteSkinPopup
    Left = 429
    Top = 32
    object mniDeleteSkin: TMenuItem
      Caption = #1059#1076#1072#1083#1080#1090#1100' '#1074#1099#1076#1077#1083#1077#1085#1085#1099#1081' '#1089#1082#1080#1085
      OnClick = mniDeleteSkinClick
    end
  end
end
