unit Displays;

interface

uses
  Windows, Forms, MultiMon;

var
  MonList: array of TMonitorInfoEx;

implementation

function GetMonitorInfo(hmon: HMONITOR): TMonitorInfoEx;
var
  DispDev: TDisplayDevice;
  monInfo: TMonitorInfoEx;
begin
  monInfo.cbSize := sizeof(monInfo);
  if GetMonitorInfoW(hmon, @monInfo) then
  begin
    DispDev.cb := sizeof(DispDev);
    EnumDisplayDevices(@monInfo.szDevice, 0, DispDev, 0);
    Result := monInfo;
  end;
end;

procedure InitMonitors;
var
  i: Integer;
begin
  SetLength(MonList, Screen.MonitorCount);
  for i := 0 to Screen.MonitorCount - 1 do
  begin
    MonList[i] := GetMonitorInfo(Screen.Monitors[i].Handle);
  end;

end;

initialization

InitMonitors;

finalization

end.
