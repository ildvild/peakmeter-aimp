object AIMPOptionsBaseFrame: TAIMPOptionsBaseFrame
  Left = 0
  Top = 0
  BorderStyle = bsNone
  ClientHeight = 400
  ClientWidth = 500
  Color = 15790320
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Visible = True
  OnPaint = FormPaint
  DesignSize = (
    500
    400)
  PixelsPerInch = 96
  TextHeight = 13
  object pbHeader: TPaintBox
    Left = 0
    Top = 0
    Width = 500
    Height = 22
    Align = alTop
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    OnPaint = pbHeaderPaint
  end
  object lblVersion: TLinkLabel
    Left = 428
    Top = 373
    Width = 62
    Height = 17
    Cursor = crHandPoint
    Anchors = [akRight, akBottom]
    Caption = '<a href="http://www.aimp.ru">'#1042#1077#1088#1089#1080#1103': x.x</a>'
    TabOrder = 0
    OnClick = lblVersionClick
  end
end
