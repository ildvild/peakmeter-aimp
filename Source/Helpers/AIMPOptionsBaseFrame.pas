unit AIMPOptionsBaseFrame;

interface

uses
  Windows, Types, Classes, Graphics, Controls, Forms, ExtCtrls,apiWrappersUI;

type
  TAIMPOptionsBaseFrame = class(TAIMPCustomOptionsFrameForm)
    pbHeader: TPaintBox;
    lblVersion: TLinkLabel;
    procedure FormPaint(Sender: TObject);
    procedure pbHeaderPaint(Sender: TObject);
    procedure lblVersionClick(Sender: TObject);
  private
    procedure SetVersion(Value: string);
  protected
    ForumUrl: string;
    PluginName: string;
  public
    property Version: string write SetVersion;
  end;

implementation

uses
  apiWrappers, Math, ShellAPI;

const
  clClickedColor: TColor = $FFD59F;
  clGradColor1: TColor = $FEFEFE;
  clGradColor2: TColor = $F5F5F5;

  {$R *.dfm}

procedure GradVertical(Canvas: TCanvas; Rect: TRect;
  FromColor, ToColor: TColor);
var
  B: Byte;
  b1: Byte;
  b2: Byte;
  C1: TColor;
  C2: TColor;
  cnt: Integer;
  db: Extended;
  dg: Extended;
  dr: Extended;
  G: Byte;
  g1: Byte;
  g2: Byte;
  R: Byte;
  r1: Byte;
  r2: Byte;
  Y: Integer;
begin
  C1 := FromColor;
  r1 := GetRValue(C1);
  g1 := GetGValue(C1);
  b1 := GetBValue(C1);

  C2 := ToColor;
  r2 := GetRValue(C2);
  g2 := GetGValue(C2);
  b2 := GetBValue(C2);

  dr := (r2 - r1) / Rect.Bottom - Rect.Top;
  dg := (g2 - g1) / Rect.Bottom - Rect.Top;
  db := (b2 - b1) / Rect.Bottom - Rect.Top;

  cnt := 0;
  for Y := Rect.Top to Rect.Bottom - 1 do
  begin
    R := r1 + Ceil(dr * cnt);
    G := g1 + Ceil(dg * cnt);
    B := b1 + Ceil(db * cnt);

    Canvas.Pen.Color := RGB(R, G, B);
    Canvas.MoveTo(Rect.Left, Y);
    Canvas.LineTo(Rect.Right, Y);
    Inc(cnt);
  end;
end;

procedure TAIMPOptionsBaseFrame.FormPaint(Sender: TObject);
begin
  Canvas.Pen.Style := psSolid;
  Canvas.Pen.Width := 1;
  Canvas.Pen.Color := RGB(188, 188, 188);
  Canvas.Rectangle(Bounds(0, 0, Width, Height));
  Canvas.Pen.Color := clWhite;
  Canvas.Rectangle(Bounds(1, 1, Width - 2, Height - 2));
end;

procedure TAIMPOptionsBaseFrame.lblVersionClick(Sender: TObject);
begin
  Assert(ForumUrl <> '', '�� ������ ��� ����� �����');
  ShellExecute(0, 'Open', PChar(ForumUrl), PChar(''), nil, SW_SHOWNORMAL);
end;

procedure TAIMPOptionsBaseFrame.pbHeaderPaint(Sender: TObject);
var
  Flags: Longint;
  pbRect: TRect;
begin
  pbRect := Rect(0, 0, pbHeader.Width, pbHeader.Height);

  GradVertical(pbHeader.Canvas, pbHeader.ClientRect, clGradColor1,
    clGradColor2);

  pbHeader.Canvas.Brush.Style := bsClear;
  pbHeader.Canvas.Pen.Style := psSolid;
  pbHeader.Canvas.Pen.Width := 1;
  pbHeader.Canvas.Pen.Color := $BCBCBC;
  pbHeader.Canvas.Rectangle(Bounds(0, 0, pbHeader.Width, pbHeader.Height));

  Flags := DT_EXPANDTABS or DT_SINGLELINE or DT_VCENTER or DT_CENTER;
  Flags := DrawTextBiDiModeFlags(Flags);
  DrawText(pbHeader.Canvas.Handle, PluginName, -1, pbRect, Flags);

end;

procedure TAIMPOptionsBaseFrame.SetVersion(Value: string);
begin
  lblVersion.Caption := Value;
end;

end.
