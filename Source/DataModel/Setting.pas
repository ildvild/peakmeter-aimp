unit Setting;

interface

uses
  apiWrappers;

type
  TPluginSetting = class
  private
    FConfig: TAIMPServiceConfig;
    FInScreenCenter: Boolean;
    FMeterTransparent: Byte;
    FMonitorIndex: Byte;
    FMovable: Boolean;
    FPositionX: Double;
    FPositionY: Double;
    FShowPeakMeter: Boolean;
    FSkinName: string;
    FStayOnTop: Boolean;
    FHideWhenStopped: Boolean;
  public
    constructor Create;
    destructor Destroy; override;
    procedure LoadSetting;
    procedure SaveSetting;
    //
    property InScreenCenter: Boolean read FInScreenCenter write FInScreenCenter;
    property MeterTransparent: Byte read FMeterTransparent write FMeterTransparent;
    property MonitorIndex: Byte read FMonitorIndex write FMonitorIndex;
    property Movable: Boolean read FMovable write FMovable;
    property PositionX: Double read FPositionX write FPositionX;
    property PositionY: Double read FPositionY write FPositionY;
    property ShowPeakMeter: Boolean read FShowPeakMeter write FShowPeakMeter;
    property SkinName: string read FSkinName write FSkinName;
    property StayOnTop: Boolean read FStayOnTop write FStayOnTop;
    property HideWhenStopped: Boolean read FHideWhenStopped write FHideWhenStopped;
  end;

var
  PluginSetting: TPluginSetting;

implementation

const
  sConfigSection = 'PeakMeter\';

  { TSetting }

constructor TPluginSetting.Create;
begin
  inherited Create;
  FConfig := TAIMPServiceConfig.Create;
end;

destructor TPluginSetting.Destroy;
begin
  FConfig.Free;
  inherited Destroy;
end;

procedure TPluginSetting.LoadSetting;
begin
  FShowPeakMeter := FConfig.ReadBool(sConfigSection + 'ShowPeakMeter', True);
  FSkinName := FConfig.ReadString(sConfigSection + 'SkinName');
  FInScreenCenter := FConfig.ReadBool(sConfigSection + 'InScreenCenter', False);
  FStayOnTop := FConfig.ReadBool(sConfigSection + 'StayOnTop', True);
  FMovable := FConfig.ReadBool(sConfigSection + 'Movable', True);
  FPositionX := FConfig.ReadFloat(sConfigSection + 'PositionX', 50);
  FPositionY := FConfig.ReadFloat(sConfigSection + 'PositionY', 50);
  FMeterTransparent := FConfig.ReadInteger(sConfigSection + 'MeterTransparent', 0);
  FMonitorIndex := FConfig.ReadInteger(sConfigSection + 'MonitorIndex', 0);
  FHideWhenStopped := FConfig.ReadBool(sConfigSection + 'HideWhenStopped', False);
end;

procedure TPluginSetting.SaveSetting;
begin
  FConfig.WriteBool(sConfigSection + 'ShowPeakMeter', FShowPeakMeter);
  FConfig.WriteString(sConfigSection + 'SkinName', FSkinName);
  FConfig.WriteBool(sConfigSection + 'InScreenCenter', FInScreenCenter);
  FConfig.WriteBool(sConfigSection + 'StayOnTop', FStayOnTop);
  FConfig.WriteBool(sConfigSection + 'Movable', FMovable);
  FConfig.WriteFloat(sConfigSection + 'PositionX', FPositionX);
  FConfig.WriteFloat(sConfigSection + 'PositionY', FPositionY);
  FConfig.WriteInteger(sConfigSection + 'MeterTransparent', FMeterTransparent);
  FConfig.WriteInteger(sConfigSection + 'MonitorIndex', FMonitorIndex);
  FConfig.WriteBool(sConfigSection + 'HideWhenStopped', FHideWhenStopped);
end;

end.
