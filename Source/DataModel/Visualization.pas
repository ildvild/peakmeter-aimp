unit Visualization;

interface

uses
  apiVisuals, GdiPlus, Types, Models;

type
  TExtensionVisualization = class(TInterfacedObject,
    IAIMPExtensionCustomVisualization)
  private
    GLayer: IGPGraphics;
    function GetImageLevelRect(SrcPng: IGPImage; const Level: Single;
      Max: Cardinal; Orientation: TMeterOrientation): TRect;
  protected
    procedure Draw(Data: PAIMPVisualData); stdcall;
    function GetFlags: Integer; stdcall;
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  OSD, Setting;

constructor TExtensionVisualization.Create;
begin
  inherited;
  PeakMeterOSD := TPeakMeterOSD.Create(nil);
  GLayer := TGPGraphics.Create
    (PeakMeterOSD.LayeredWindow.Surface.Canvas.Handle);

  if Assigned(CurrentMeterSkin.BackPng) then
  begin
    PeakMeterOSD.Width := CurrentMeterSkin.BackPng.Width;
    PeakMeterOSD.Height := CurrentMeterSkin.BackPng.Height;
    PeakMeterOSD.LayeredWindow.Surface.SetSize(CurrentMeterSkin.BackPng.Width,
      CurrentMeterSkin.BackPng.Height);
  end;

  PeakMeterOSD.ApplySetting;
end;

destructor TExtensionVisualization.Destroy;
begin
  GLayer := nil;
  PeakMeterOSD.Free;
  inherited Destroy;
end;

procedure TExtensionVisualization.Draw(Data: PAIMPVisualData);
var
  r: TRect;
begin

  if not PluginSetting.ShowPeakMeter or not Assigned(CurrentMeterSkin.Skin) or
    not(PeakMeterOSD.Visible) then
    Exit;

  GLayer := TGPGraphics.Create
    (PeakMeterOSD.LayeredWindow.Surface.Canvas.Handle);
  try
    GLayer.Clear($00FFFFFF);

    if Assigned(CurrentMeterSkin.BackPng) then
      GLayer.DrawImage(CurrentMeterSkin.BackPng, 0, 0,
        CurrentMeterSkin.BackPng.Width, CurrentMeterSkin.BackPng.Height);

    case CurrentMeterSkin.Skin.OSD.MeterStyle of
      Stereo:
        begin

          r := GetImageLevelRect(CurrentMeterSkin.MeterLeftPng, Data^.Peaks[0],
            CurrentMeterSkin.Skin.OSD.MeterMax,
            CurrentMeterSkin.Skin.OSD.MeterOrientation);

          GLayer.DrawImage(CurrentMeterSkin.MeterLeftPng,
            CurrentMeterSkin.Skin.OSD.MeterLeftPosition.X, // X
            CurrentMeterSkin.Skin.OSD.MeterLeftPosition.Y, // Y
            r.Right - r.Left, // ������
            r.Bottom - r.Top, // ������
            r.Left, r.Top, r.Right - r.Left, r.Bottom - r.Top, UnitPixel);

          r := GetImageLevelRect(CurrentMeterSkin.MeterRightPng, Data^.Peaks[1],
            CurrentMeterSkin.Skin.OSD.MeterMax,
            CurrentMeterSkin.Skin.OSD.MeterOrientation);

          GLayer.DrawImage(CurrentMeterSkin.MeterRightPng,
            CurrentMeterSkin.Skin.OSD.MeterRightPosition.X, // X
            CurrentMeterSkin.Skin.OSD.MeterRightPosition.Y, // Y
            r.Right - r.Left, // ������
            r.Bottom - r.Top, // ������
            r.Left, r.Top, r.Right - r.Left, r.Bottom - r.Top, UnitPixel);

        end;

      Mono:
        begin
          //
          // if CurrentMeterSkin.Skin.OSD.MeterOrientation = Vertical then
          // begin
          // GLayer.TranslateTransform(CurrentMeterSkin.MeterLeftPng.Width +
          // CurrentMeterSkin.Skin.OSD.MeterLeftPosition.X,
          // CurrentMeterSkin.MeterLeftPng.Height -
          // CurrentMeterSkin.Skin.OSD.MeterLeftPosition.Y);
          // GLayer.RotateTransform(180);
          // end;

          r := GetImageLevelRect(CurrentMeterSkin.MeterLeftPng, Data^.Peaks[0],
            CurrentMeterSkin.Skin.OSD.MeterMax,
            CurrentMeterSkin.Skin.OSD.MeterOrientation);

          GLayer.DrawImage(CurrentMeterSkin.MeterLeftPng,
            CurrentMeterSkin.Skin.OSD.MeterLeftPosition.X, // X
            CurrentMeterSkin.Skin.OSD.MeterLeftPosition.Y, // Y
            r.Right - r.Left, // ������
            r.Bottom - r.Top, // ������
            r.Left, r.Top, r.Right - r.Left, r.Bottom - r.Top, UnitPixel);

        end;

    end;

    if Assigned(CurrentMeterSkin.GlassPng) then
      GLayer.DrawImage(CurrentMeterSkin.GlassPng, 0, 0,
        CurrentMeterSkin.GlassPng.Width, CurrentMeterSkin.GlassPng.Height);

    if Assigned(CurrentMeterSkin.GlowPng) then
      GLayer.DrawImage(CurrentMeterSkin.GlowPng, 0, 0,
        CurrentMeterSkin.GlowPng.Width, CurrentMeterSkin.GlowPng.Height);

    PeakMeterOSD.LayeredWindow.UpdateLayer;
  finally
    GLayer := nil;
  end;

end;

function TExtensionVisualization.GetFlags: Integer;
begin
  Result := AIMP_VISUAL_FLAGS_NOT_SUSPEND;
end;

function TExtensionVisualization.GetImageLevelRect(SrcPng: IGPImage;
  const Level: Single; Max: Cardinal; Orientation: TMeterOrientation): TRect;
var
  dX: Cardinal;
  dY: Cardinal;
  X1: Cardinal;
  X2: Cardinal;
  Y1: Cardinal;
  Y2: Cardinal;
  nLevel: Single;
begin
  Result := Rect(0, 0, 0, 0);

  if not Assigned(SrcPng) then
    Exit;

  if (SrcPng.Width = 0) or (SrcPng.Height = 0) then
    Exit;

  if Level > 1 then
    nLevel := 1
  else
    nLevel := Level;

  case Orientation of
    BitStripRight:
      begin
        dX := SrcPng.Width div (Max + 1);

        X1 := Round((SrcPng.Width * nLevel) / dX) * dX;
        if X1 = dX * (Max + 1) then
          X1 := X1 - dX;
        X2 := X1 + dX;

        Result := Rect(X1, 0, X2, SrcPng.Height);
      end;
    BitStripBottom:
      begin
        dY := SrcPng.Height div (Max + 1);

        Y1 := Round((SrcPng.Height * nLevel) / dY) * dY;
        if Y1 = dY * (Max + 1) then
          Y1 := Y1 - dY;
        Y2 := Y1 + dY;
        Result := Rect(0, Y1, SrcPng.Width, Y2);
      end;
    Horizontal:
      begin
        dX := SrcPng.Width div Max;
        X2 := Round((SrcPng.Width * nLevel) / dX) * dX;

        if X2 <> 0 then
          Result := Rect(0, 0, X2, SrcPng.Height);
      end;
    Vertical:
      begin
        dY := SrcPng.Height div Max;
        Y2 := Round((SrcPng.Height * nLevel) / dY) * dY;

        if Y2 <> 0 then
          Result := Rect(0, 0, SrcPng.Width, Y2);
      end;

  end;

end;

end.
