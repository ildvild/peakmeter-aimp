unit Models;

interface

uses
  Types, SysUtils, Classes, GdiPlus;

type
  TMeterOrientation = (BitStripRight, BitStripBottom, Horizontal, Vertical);

  TMeterStyle = (Stereo, Mono);

  TMeterSkinInfo = packed record
    Author: string;
    Website: string;
  end;

  TMeterSkinOsd = packed record
    MeterOrientation: TMeterOrientation;
    MeterMax: Integer;
    MeterStyle: TMeterStyle;
    MeterLeftPosition: TPoint;
    MeterRightPosition: TPoint;
  end;

  TMeterSkinPathFiles = packed record
    BackPng: string;
    MeterLeftPng: string;
    MeterRightPng: string;
    GlassPng: string;
    GlowPng: string;
    PreviewPng: string;
    MeterAllPng: string;
  end;

  TMeterSkin = class
  strict private
    FFiles: TMeterSkinPathFiles;
    FInfo: TMeterSkinInfo;
    FName: string; // �������� ����� = �������� �����
    FOsd: TMeterSkinOsd;
    FPath: string;
    procedure GetSkinInfo(const APath: string);
    procedure SetPath(const APath: string);
  public
    property Files: TMeterSkinPathFiles read FFiles write FFiles;
    property Info: TMeterSkinInfo read FInfo write FInfo;
    property Name: string read FName write FName;
    property Osd: TMeterSkinOsd read FOsd write FOsd;
    property Path: string read FPath write SetPath;
  end;

  TMeterSkinItem = class(TCollectionItem)
  private
    FMeterSkin: TMeterSkin;
    procedure SetMeterSkin(const Value: TMeterSkin);
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    property MeterSkin: TMeterSkin read FMeterSkin write SetMeterSkin;
  end;

  TMeterSkinList = class(TCollection)
  private
    function GetItemMeterSkin(Index: Integer): TMeterSkin;
    procedure SetItemMeterSkin(Index: Integer; const Value: TMeterSkin);
  public
    constructor Create(const Path: string);
    function Find(const AName: string): TMeterSkin; overload;
    function Add: TMeterSkin;
    property Items[Index: Integer]: TMeterSkin read GetItemMeterSkin
      write SetItemMeterSkin; default;
  end;

  TBufferMeterSkin = class
  public
    BackPng: IGPImage;
    GlassPng: IGPImage;
    GlowPng: IGPImage;

    MeterLeftPng: IGPImage;
    MeterRightPng: IGPImage;

    Skin: TMeterSkin;
    constructor Create;
    destructor Destroy; override;
    procedure GetData(const AData: TMeterSkin);
  end;

var
  CurrentMeterSkin: TBufferMeterSkin;

function MeterOrientationToStr(AMeterOrientation: TMeterOrientation): string;

implementation

uses
  XMLIntf, XMLDoc, GlobalData;

function MeterOrientationToStr(AMeterOrientation: TMeterOrientation): string;
begin
  case AMeterOrientation of
    BitStripRight:
      Result := 'BitStripRight';
    BitStripBottom:
      Result := 'BitStripBottom';
    Horizontal:
      Result := 'Horizontal';
    Vertical:
      Result := 'Vertical';
  else
    Result := '';
  end;
end;

{ TMeterSkin }

procedure TMeterSkin.GetSkinInfo(const APath: string);
var
  XMLDoc: IXMLDocument;
  nInfo: IXmlNode;
  nOsd: IXmlNode;
  nPosition: IXmlNode;
  str: string;
begin

  XMLDoc := TXMLDocument.Create(nil);
  try

    XMLDoc.LoadFromFile(APath);
    XMLDoc.Active := True;
    try

      nInfo := XMLDoc.DocumentElement.ChildNodes.FindNode('info');
      try
        FInfo.Author := nInfo.ChildNodes['author'].Text;
        FInfo.Website := nInfo.ChildNodes['website'].Text;
      finally
        nInfo := nil;
      end;

      nOsd := XMLDoc.DocumentElement.ChildNodes.FindNode('osd');
      try

        str := nOsd.ChildNodes['meterStyle'].Text;
        if LowerCase(str) = 'mono' then
          FOsd.MeterStyle := Mono
        else if LowerCase(str) = 'stereo' then
          FOsd.MeterStyle := Stereo;
        //
        str := nOsd.ChildNodes['meterMax'].Text;
        FOsd.MeterMax := Strtoint(str);
        //
        str := nOsd.ChildNodes['meterOrientation'].Text;
        if LowerCase(str) = 'horizontal' then
          FOsd.MeterOrientation := Horizontal
        else if LowerCase(str) = 'vertical' then
          FOsd.MeterOrientation := Vertical
        else if LowerCase(str) = 'bitstripright' then
          FOsd.MeterOrientation := BitStripRight
        else if ((LowerCase(str) = 'bitstripbottom') or
          (LowerCase(str) = 'bitstrip')) then
          FOsd.MeterOrientation := BitStripBottom;

        nPosition := nOsd.ChildNodes.FindNode('meterPosition');
        try

          if Assigned(nPosition) then
          begin
            str := nPosition.ChildNodes['X'].Text;
            FOsd.MeterLeftPosition.X := Strtoint(str);
            str := nPosition.ChildNodes['Y'].Text;
            FOsd.MeterLeftPosition.Y := Strtoint(str);
          end;
        finally
          nPosition := nil;
        end;

        nPosition := nOsd.ChildNodes.FindNode('meterLeftPosition');
        try

          if Assigned(nPosition) then
          begin
            str := nPosition.ChildNodes['X'].Text;
            FOsd.MeterLeftPosition.X := Strtoint(str);
            str := nPosition.ChildNodes['Y'].Text;
            FOsd.MeterLeftPosition.Y := Strtoint(str);
          end;
        finally
          nPosition := nil;
        end;

        nPosition := nOsd.ChildNodes.FindNode('meterRightPosition');
        try

          if Assigned(nPosition) then
          begin
            str := nPosition.ChildNodes['X'].Text;
            FOsd.MeterRightPosition.X := Strtoint(str);
            str := nPosition.ChildNodes['Y'].Text;
            FOsd.MeterRightPosition.Y := Strtoint(str);
          end;

        finally
          nPosition := nil;
        end;

      finally
        nOsd := nil;
      end;

    finally
      XMLDoc.Active := false;
    end;

  finally
    XMLDoc := nil;
  end;
end;

procedure TMeterSkin.SetPath(const APath: string);
begin
  FPath := APath;

  FName := ExtractFilename(APath);
  FFiles.PreviewPng := APath + '\Preview.png';
  FFiles.BackPng := APath + '\Back.png';
  FFiles.GlassPng := APath + '\Glass.png';
  FFiles.GlowPng := APath + '\Glow.png';
  FFiles.MeterLeftPng := APath + '\Meter_Left.png';
  FFiles.MeterRightPng := APath + '\Meter_Right.png';
  FFiles.MeterAllPng := APath + '\Meter_All.png';

  GetSkinInfo(APath + '\Skin.xml');
end;

{ TBufferMeterSkin }

constructor TBufferMeterSkin.Create;
begin
  inherited;
end;

destructor TBufferMeterSkin.Destroy;
begin
  BackPng := nil;
  MeterLeftPng := nil;
  MeterRightPng := nil;
  GlassPng := nil;
  GlowPng := nil;
  inherited;
end;

procedure TBufferMeterSkin.GetData(const AData: TMeterSkin);
begin

  if Assigned(AData) then
  begin
    Skin := AData;

    BackPng := nil;
    MeterLeftPng := nil;
    MeterRightPng := nil;
    GlassPng := nil;
    GlowPng := nil;

    if FileExists(AData.Files.BackPng) then
      BackPng := TGPImage.Create(AData.Files.BackPng);

    if FileExists(AData.Files.MeterAllPng) then
    begin
      MeterLeftPng := TGPImage.Create(AData.Files.MeterAllPng);
      MeterRightPng := TGPImage.Create(AData.Files.MeterAllPng);
      if Skin.Osd.MeterOrientation = Vertical then
      begin
        MeterLeftPng.RotateFlip(Rotate180FlipNone);
        MeterRightPng.RotateFlip(Rotate180FlipNone);
      end;
    end
    else
    begin
      if FileExists(AData.Files.MeterLeftPng) then
      begin
        MeterLeftPng := TGPImage.Create(AData.Files.MeterLeftPng);
        if Skin.Osd.MeterOrientation = Vertical then
          MeterLeftPng.RotateFlip(Rotate180FlipNone);
      end;

      if FileExists(AData.Files.MeterRightPng) then
      begin
        MeterRightPng := TGPImage.Create(AData.Files.MeterRightPng);
        if Skin.Osd.MeterOrientation = Vertical then
          MeterRightPng.RotateFlip(Rotate180FlipNone);
      end;
    end;

    if FileExists(AData.Files.GlassPng) then
      GlassPng := TGPImage.Create(AData.Files.GlassPng);

    if FileExists(AData.Files.GlowPng) then
      GlowPng := TGPImage.Create(AData.Files.GlowPng);

  end;

end;

{ TMeterSkinItem }

constructor TMeterSkinItem.Create(Collection: TCollection);
begin
  inherited;
  FMeterSkin := TMeterSkin.Create;
end;

destructor TMeterSkinItem.Destroy;
begin
  FMeterSkin.Free;
  inherited;
end;

procedure TMeterSkinItem.SetMeterSkin(const Value: TMeterSkin);
begin
  FMeterSkin := Value;
end;

{ TMeterSkinList }

function TMeterSkinList.Add: TMeterSkin;
begin
  Result := TMeterSkinItem( inherited Add).MeterSkin;
end;

constructor TMeterSkinList.Create(const Path: string);

  function IsValidDir(SearchRec: TSearchRec): Boolean;
  begin
    if (((SearchRec.Attr >= 16) and (SearchRec.Attr <= 31)) or
      (SearchRec.Attr = 48) or (SearchRec.Attr = 49) or (SearchRec.Attr = 50) or
      (SearchRec.Attr = 2064) or (SearchRec.Attr = 2066) or
      (SearchRec.Attr = 2096) or (SearchRec.Attr = 2098) or
      (SearchRec.Attr = 8208) or (SearchRec.Attr = 8210) or
      (SearchRec.Attr = 8240) or (SearchRec.Attr = 8242) or
      (SearchRec.Attr = 10256) or (SearchRec.Attr = 10258) or
      (SearchRec.Attr = 10288) or (SearchRec.Attr = 10290) or
      (SearchRec.Attr = 16400) or (SearchRec.Attr = 16402) or
      (SearchRec.Attr = 16432) or (SearchRec.Attr = 16434) or
      (SearchRec.Attr = 24624) or (SearchRec.Attr = 24626) or
      (SearchRec.Attr = 1243048)) and (SearchRec.Name <> '.') and
      (SearchRec.Name <> '..') then
      Result := True
    else
      Result := false;
  end;

var
  SkinPath: string;
  SR: TSearchRec;
begin
  inherited Create(TMeterSkinItem);

  SkinPath := Path + SInfoName + '\' + SSkinFolderName + '\';
  if DirectoryExists(SkinPath) then
    if FindFirst(SkinPath + '*.*', faDirectory, SR) = 0 then
    begin
      repeat
        if IsValidDir(SR) then
        begin
          Add.Path := SkinPath + SR.Name;
        end;
      until FindNext(SR) <> 0;
      FindClose(SR);
    end;
end;

function TMeterSkinList.Find(const AName: string): TMeterSkin;
var
  i: Integer;
begin
  Result := nil;

  for i := 0 to Count - 1 do
  begin
    if GetItemMeterSkin(i).Name = AName then
    begin
      Result := GetItemMeterSkin(i);
      Exit;
    end;
  end;

  if Count <> 0 then
    Result := GetItemMeterSkin(0);
end;

function TMeterSkinList.GetItemMeterSkin(Index: Integer): TMeterSkin;
begin
  Result := TMeterSkinItem( inherited Items[Index]).MeterSkin;
end;

procedure TMeterSkinList.SetItemMeterSkin(Index: Integer;
  const Value: TMeterSkin);
begin
  TMeterSkinItem( inherited Items[Index]).MeterSkin := Value;
end;

initialization

CurrentMeterSkin := TBufferMeterSkin.Create;

finalization

CurrentMeterSkin.Free;

end.
