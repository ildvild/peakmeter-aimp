{ TODO -oOwner -cGeneral : Добавить Удалить выделенный скин }

unit Plugin;

interface

uses
  Windows, apiCore, apiMenu, apiActions, apiObjects, apiVisuals, apiPlugin, apiMessages,
  apiOptions, AIMPCustomPlugin, apiWrappers, apiPlaylists, Visualization, Models;

type
  { TMeterPlugin }
  TMeterPlugin = class;

  { TPMActionBeforeMenuShow }
  TPMActionBeforeMenuShow = class(TInterfacedObject, IAIMPActionEvent)
  private
    FAShowSettingMenuItem: IAIMPMenuItem; // указатель на пункт меню
  public
    procedure OnExecute(Data: IInterface); stdcall;
    constructor Create(Value: IAIMPMenuItem);
    destructor Destroy; override;
  end;

  { TAIMPHook }
  TAIMPHook = class(TInterfacedObject, IAIMPMessageHook)
  private
    FPlugin: TMeterPlugin;
    procedure CoreMessage(Message: DWORD; Param1: Integer; Param2: Pointer;
      var Result: HRESULT); stdcall;
  public
    property Plugin: TMeterPlugin read FPlugin write FPlugin;
  end;

  { TMeterPlugin }
  TMeterPlugin = class(TAIMPCustomPlugin)
  private
    AShowSettingAction: IAIMPAction; // This Action is show Plugin's Setting Tab
    AShowHideAction: IAIMPAction; // This Action is show/hide drawing form.
    AShowSettingMenuItem: IAIMPMenuItem;
    PMActionBeforeMenuShow: TPMActionBeforeMenuShow;
    ExtensionVisualization: TExtensionVisualization;
    Hook: TAIMPHook;
    FMeterSkinList: TMeterSkinList;
    procedure CreateMenuItems;
    procedure CreateShowHideAction;
    procedure CreateShowSettingAction;
    function GetBuiltInMenu(ID: Integer): IAIMPMenuItem;
  protected
    procedure Finalize; override; stdcall;
    function InfoGet(Index: Integer): PWideChar; override; stdcall;
    function InfoGetCategories: Cardinal; override; stdcall;
    function Initialize(Core: IAIMPCore): HRESULT; override; stdcall;
  public
    procedure SetLanguage;
    property MeterSkinList: TMeterSkinList read FMeterSkinList write FMeterSkinList;
  end;

implementation

uses
  Classes, Setting, AIMPOptionsFramePeakMeter, OSD, GlobalData;

type
  TPMShowHideHandler = class(TInterfacedObject, IAIMPActionEvent)
  public
    procedure OnExecute(Data: IInterface); stdcall;
  end;

  TPMShowSettingHandler = class(TInterfacedObject, IAIMPActionEvent)
  public
    procedure OnExecute(Data: IInterface); stdcall;
  end;

function CreateGlyph(const ResName: string): IAIMPImage;
var
  AContainer: IAIMPImageContainer;
  AResStream: TResourceStream;
  AVerInfo: IAIMPServiceVersionInfo;
begin
  if CoreGetService(IID_IAIMPServiceVersionInfo, AVerInfo) then
  begin
    if AVerInfo.GetBuildNumber < 1683 then
      Exit(nil); //#AI - Older version of API has an incorrect definition of the IAIMPImageContainer.SetDataSize method
  end;
  CoreCreateObject(IID_IAIMPImageContainer, AContainer);
  AResStream := TResourceStream.Create(HInstance, ResName, RT_RCDATA);
  try
    CheckResult(AContainer.SetDataSize(AResStream.Size));
    AResStream.ReadBuffer(AContainer.GetData^, AContainer.GetDataSize);
    CheckResult(AContainer.CreateImage(Result));
  finally
    AResStream.Free;
  end;
end;

{ TVisualPlugin }
procedure TMeterPlugin.CreateMenuItems;
begin
  // Create AShowHideMenuItem
  CoreCreateObject(IID_IAIMPMenuItem, AShowSettingMenuItem);

  PMActionBeforeMenuShow := TPMActionBeforeMenuShow.Create(AShowSettingMenuItem);
  // Setup it
  // 1. Set unique ID for menu item
  CheckResult(AShowSettingMenuItem.SetValueAsObject(AIMP_MENUITEM_PROPID_ID,
    MakeString('PeakMeter.ShowHideAction')));
  // 2. Set action for menu item
  CheckResult(AShowSettingMenuItem.SetValueAsObject(AIMP_MENUITEM_PROPID_ACTION, AShowHideAction));
  // 3. Set parent item for menu item
  CheckResult(AShowSettingMenuItem.SetValueAsObject(AIMP_MENUITEM_PROPID_PARENT,
    GetBuiltInMenu(AIMP_MENUID_COMMON_UTILITIES)));
  // 4. Set style item for menu item
  CheckResult(AShowSettingMenuItem.SetValueAsInt32(AIMP_MENUITEM_PROPID_STYLE,
    AIMP_MENUITEM_STYLE_NORMAL));
  // 5. Set icon for menu item
  CheckResult(AShowSettingMenuItem.SetValueAsObject(AIMP_MENUITEM_PROPID_GLYPH,
    CreateGlyph('Icon_On')));
  // 6. Set  caption (Name)
  CheckResult(AShowSettingMenuItem.SetValueAsObject(AIMP_MENUITEM_PROPID_NAME,
    MakeString(LangLoadString('PeakMeter\Name'))));
  // 7. Set  onshow event
  CheckResult(AShowSettingMenuItem.SetValueAsObject(AIMP_MENUITEM_PROPID_EVENT_ONSHOW,
    PMActionBeforeMenuShow));

  // Register the menu item in manager
  CoreIntf.RegisterExtension(IID_IAIMPServiceMenuManager, AShowSettingMenuItem);

end;

procedure TMeterPlugin.CreateShowHideAction;
var
  aimpServiceActionManager: IAIMPServiceActionManager;
begin
  if CoreGetService(IID_IAIMPServiceActionManager, aimpServiceActionManager) then
    try
      // Create Action
      CoreCreateObject(IID_IAIMPAction, AShowHideAction);
      // Setup it
      CheckResult(AShowHideAction.SetValueAsObject(AIMP_ACTION_PROPID_ID,
        MakeString('PeakMeter.ShowHideAction')));
      CheckResult(AShowHideAction.SetValueAsObject(AIMP_ACTION_PROPID_NAME,
        MakeString(LangLoadString('PeakMeter\ShowHide'))));
      CheckResult(AShowHideAction.SetValueAsObject(AIMP_ACTION_PROPID_GROUPNAME,
        MakeString(LangLoadString('PeakMeter\Name'))));
      CheckResult(AShowHideAction.SetValueAsObject(AIMP_ACTION_PROPID_EVENT,
        TPMShowHideHandler.Create));
      CheckResult(AShowHideAction.SetValueAsInt32(AIMP_ACTION_PROPID_ENABLED, 1));
      // Register the action in manager
      CoreIntf.RegisterExtension(IID_IAIMPServiceActionManager, AShowHideAction);
    finally
      aimpServiceActionManager := nil;
    end;
end;

procedure TMeterPlugin.CreateShowSettingAction;
var
  aimpServiceActionManager: IAIMPServiceActionManager;
begin
  if CoreGetService(IID_IAIMPServiceActionManager, aimpServiceActionManager) then
    try
      // Create Action
      CoreCreateObject(IID_IAIMPAction, AShowSettingAction);
      // Setup it
      CheckResult(AShowSettingAction.SetValueAsObject(AIMP_ACTION_PROPID_ID,
        MakeString('PeakMeter.ShowSettingAction')));
      CheckResult(AShowSettingAction.SetValueAsObject(AIMP_ACTION_PROPID_NAME,
        MakeString(LangLoadString('PeakMeter\Setting'))));
      CheckResult(AShowSettingAction.SetValueAsObject(AIMP_ACTION_PROPID_GROUPNAME,
        MakeString(LangLoadString('PeakMeter\Name'))));
      CheckResult(AShowSettingAction.SetValueAsObject(AIMP_ACTION_PROPID_EVENT,
        TPMShowSettingHandler.Create));
      CheckResult(AShowSettingAction.SetValueAsInt32(AIMP_ACTION_PROPID_ENABLED, 1));
      // Register the action in manager
      CoreIntf.RegisterExtension(IID_IAIMPServiceActionManager, AShowSettingAction);
    finally
      aimpServiceActionManager := nil;
    end;
end;

procedure TMeterPlugin.Finalize;
var
  AMessageService: IAIMPServiceMessageDispatcher;
begin
  OptionFrame := nil;
  ExtensionVisualization := nil;
  PluginSetting.SaveSetting;
  PluginSetting.Free;
  FMeterSkinList.Free;

  if CoreGetService(IID_IAIMPServiceMessageDispatcher, AMessageService) then
    AMessageService.Unhook(Hook);

  Hook := nil;
  PMActionBeforeMenuShow.FAShowSettingMenuItem := nil;
  PMActionBeforeMenuShow := nil;
  AShowHideAction := nil;
  AShowSettingAction := nil;
  AShowSettingMenuItem := nil;

  inherited;
end;

function TMeterPlugin.GetBuiltInMenu(ID: Integer): IAIMPMenuItem;
var
  AMenuService: IAIMPServiceMenuManager;
begin
  CoreGetService(IAIMPServiceMenuManager, AMenuService);
  CheckResult(AMenuService.GetBuiltIn(ID, Result));
end;

function TMeterPlugin.InfoGet(Index: Integer): PWideChar;
begin
  case Index of
    AIMP_PLUGIN_INFO_NAME:
      Result := SInfoName;
    AIMP_PLUGIN_INFO_AUTHOR:
      Result := SAuthor;
    AIMP_PLUGIN_INFO_SHORT_DESCRIPTION:
      Result := SDescription;
    AIMP_PLUGIN_INFO_FULL_DESCRIPTION:
      Result := SFullDescription;
  else
    Result := nil;
  end;
end;

function TMeterPlugin.InfoGetCategories: Cardinal;
begin
  Result := AIMP_PLUGIN_CATEGORY_ADDONS;
end;

function TMeterPlugin.Initialize(Core: IAIMPCore): HRESULT;
var
  AService: IAIMPServicePlaylistManager;
  AMessageService: IAIMPServiceMessageDispatcher;
begin
  // Check, if the Menu Service supported by core
  Result := Core.QueryInterface(IID_IAIMPServicePlaylistManager, AService);
  if Succeeded(Result) then
  begin
    Result := inherited Initialize(Core);
    if Succeeded(Result) then
    begin

      // Создаем объект, хранящий настройки
      PluginSetting := TPluginSetting.Create;
      PluginSetting.LoadSetting;

      //
      FMeterSkinList := TMeterSkinList.Create(GetPluginPath);
      CurrentMeterSkin.GetData(FMeterSkinList.Find(PluginSetting.SkinName));

      //
      ExtensionVisualization := TExtensionVisualization.Create;
      OptionFrame := TOptionFrame.Create;
      OptionFrame.Plugin := Self;
      Core.RegisterExtension(IID_IAIMPServiceVisualizations, ExtensionVisualization);
      Core.RegisterExtension(IID_IAIMPServiceOptionsDialog, OptionFrame);

      // Создаем меню
      CreateShowHideAction;
      CreateShowSettingAction;
      CreateMenuItems;

      // ставим хук на изменения локализации
      Hook := TAIMPHook.Create;
      Hook.Plugin := Self;
      // Hook.CoreMessage:=CoreMess;
      if CoreGetService(IID_IAIMPServiceMessageDispatcher, AMessageService) then
        AMessageService.Hook(Hook);

    end;
  end;
end;

procedure TMeterPlugin.SetLanguage;
var
  aimpServiceActionManager: IAIMPServiceActionManager;
begin
  if CoreGetService(IID_IAIMPServiceActionManager, aimpServiceActionManager) then
    try

      if Assigned(AShowSettingAction) then
      begin
        CheckResult(AShowSettingAction.SetValueAsObject(AIMP_ACTION_PROPID_NAME,
          MakeString(LangLoadString('PeakMeter\Setting'))));

        CheckResult(AShowSettingAction.SetValueAsObject(AIMP_ACTION_PROPID_GROUPNAME,
          MakeString(LangLoadString('PeakMeter\Name'))));
      end;

      if Assigned(AShowHideAction) then
      begin
        CheckResult(AShowHideAction.SetValueAsObject(AIMP_ACTION_PROPID_NAME,
          MakeString(LangLoadString('PeakMeter\ShowHide'))));

        CheckResult(AShowHideAction.SetValueAsObject(AIMP_ACTION_PROPID_GROUPNAME,
          MakeString(LangLoadString('PeakMeter\Name'))));
      end;

    finally
      aimpServiceActionManager := nil;
    end;

  CheckResult(AShowSettingMenuItem.SetValueAsObject(AIMP_MENUITEM_PROPID_NAME,
    MakeString(LangLoadString('PeakMeter\Name'))));

end;

{ TPMShowHideHandler }

procedure TPMShowHideHandler.OnExecute(Data: IInterface);
begin
  if not PeakMeterOSD.isHideWhenStopped then
  begin
    PluginSetting.ShowPeakMeter := not(PluginSetting.ShowPeakMeter);
    PeakMeterOSD.Visible := not(PeakMeterOSD.Visible);
  end;
end;

{ TAIMPHook }

procedure TAIMPHook.CoreMessage(Message: DWORD; Param1: Integer; Param2: Pointer;
  var Result: HRESULT);
begin
  begin
    case Message of
      AIMP_MSG_EVENT_LANGUAGE:
        begin
          if Assigned(Plugin) then
            Plugin.SetLanguage;
          if Assigned(PeakMeterOSD) then
            PeakMeterOSD.UpdateLanguage;

        end;
      AIMP_MSG_EVENT_PLAYER_STATE:
        begin
          if PluginSetting.ShowPeakMeter then
            if PluginSetting.HideWhenStopped then
              case Param1 of
                0, 1:
                  begin
                    ShowWindow(PeakMeterOSD.handle, SW_HIDE);
                    PeakMeterOSD.isHideWhenStopped := True;
                  end;
                2:
                  begin
                    ShowWindow(PeakMeterOSD.handle, sw_showna);
                    PeakMeterOSD.isHideWhenStopped := False;
                  end;

              end;
        end;
    end;
  end;
end;

{ TPMShowSettingHandler }

procedure TPMShowSettingHandler.OnExecute(Data: IInterface);
begin
  OptionFrame.DoShow;
end;

{ TPMActionBeforeMenuShow }

constructor TPMActionBeforeMenuShow.Create(Value: IAIMPMenuItem);
begin
  FAShowSettingMenuItem := Value;
end;

destructor TPMActionBeforeMenuShow.Destroy;
begin
  FAShowSettingMenuItem := nil;
  inherited;
end;

procedure TPMActionBeforeMenuShow.OnExecute(Data: IInterface);
begin
  if not Assigned(FAShowSettingMenuItem) then
    Exit;

  if PluginSetting.ShowPeakMeter then
    CheckResult(FAShowSettingMenuItem.SetValueAsObject(AIMP_MENUITEM_PROPID_GLYPH,
      CreateGlyph('Icon_On')))
  else
    CheckResult(FAShowSettingMenuItem.SetValueAsObject(AIMP_MENUITEM_PROPID_GLYPH,
      CreateGlyph('Icon_Off')));
end;

end.
