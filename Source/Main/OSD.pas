unit OSD;

{ TODO -oOwner -cGeneral : Popup Menu �� Action menu ��� ����� ����� }
interface

uses
  Windows, Messages, Classes, Controls, Forms, Menus, LayeredWindow;

type
  TPeakMeterOSD = class(TForm)
    mniSetting: TMenuItem;
    mniMovable: TMenuItem;
    mniStayOnTop: TMenuItem;
    mniSplit: TMenuItem;
    mniHide: TMenuItem;
    pmOSD: TPopupMenu;
    procedure FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState;
      X, Y: Integer);
    procedure FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure mniHideClick(Sender: TObject);
    procedure mniSettingClick(Sender: TObject);
    procedure mniMovableClick(Sender: TObject);
    procedure mniStayOnTopClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
    procedure FormMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint;
      var Handled: Boolean);
    procedure FormMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint;
      var Handled: Boolean);
    procedure pmOSDPopup(Sender: TObject);
  private
    FLayeredWindow: TLayeredWindow;
    procedure SetStayOnTopAndPosition(const Value: Boolean);
    procedure SetInScreenCenter(const Value: Boolean);
    procedure SetMovable(const Value: Boolean);
    procedure SetMeterTransparent(const Value: Byte);
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public
    isHideWhenStopped: Boolean;
    procedure ApplySetting;
    procedure UpdateLanguage;
    property LayeredWindow: TLayeredWindow read FLayeredWindow write FLayeredWindow;
    //
    property InScreenCenter: Boolean write SetInScreenCenter;
    property MeterTransparent: Byte write SetMeterTransparent;
    property StayOnTopAndPosition: Boolean write SetStayOnTopAndPosition;
  end;

var
  PeakMeterOSD: TPeakMeterOSD;

implementation

{$R *.dfm}

uses Setting, AIMPOptionsFramePeakMeter, apiWrappers, apiPlayer, apiMessages;

procedure PauseResumePlayer;
var
  AIMPServicePlayer: IAIMPServicePlayer;
begin

  CoreGetService(IID_IAIMPServicePlayer, AIMPServicePlayer);
  try
    AIMPServicePlayer.Pause;
  finally
    AIMPServicePlayer := nil;
  end;

end;

procedure SetVolume(addValue: Single);
var
  vol: Single;
  AMessageService: IAIMPServiceMessageDispatcher;
begin
  if CoreGetService(IID_IAIMPServiceMessageDispatcher, AMessageService) then
  begin
    AMessageService.Send(AIMP_MSG_PROPERTY_VOLUME, AIMP_MSG_PROPVALUE_GET, @vol);

    vol := vol + addValue;
    if vol < 0 then
      vol := 0;

    if vol > 1 then
      vol := 1;

    AMessageService.Send(AIMP_MSG_PROPERTY_VOLUME, AIMP_MSG_PROPVALUE_SET, @vol);
  end;
end;

procedure TPeakMeterOSD.ApplySetting;
begin
  SetStayOnTopAndPosition(PluginSetting.StayOnTop);
  SetInScreenCenter(PluginSetting.InScreenCenter);
  SetMovable(PluginSetting.Movable);
  SetMeterTransparent(PluginSetting.MeterTransparent);
  Visible := PluginSetting.ShowPeakMeter;
end;

procedure TPeakMeterOSD.CreateParams(var Params: TCreateParams);
begin
  inherited;
  // ������ ����� �� ������ windows
  with Params do
    ExStyle := ExStyle OR WS_EX_TOOLWINDOW;
end;

procedure TPeakMeterOSD.FormCreate(Sender: TObject);
begin
  FLayeredWindow := TLayeredWindow.Create(Self);
  FLayeredWindow.Parent := Self;
  isHideWhenStopped := False;
end;

procedure TPeakMeterOSD.FormDblClick(Sender: TObject);
begin
  PauseResumePlayer;
end;

procedure TPeakMeterOSD.FormMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
const
  SC_DRAGMOVE = $F012;
begin
  if PluginSetting.Movable and not(PluginSetting.InScreenCenter) then
  begin
    ReleaseCapture;
    perform(WM_SYSCOMMAND, SC_DRAGMOVE, 0);
    FormMouseUp(Sender, Button, Shift, X, Y);
  end;
end;

procedure TPeakMeterOSD.FormMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer);
begin
  if ssRight in Shift then
    exit;

  if PluginSetting.Movable then
  begin
    // todo  ���������
    PluginSetting.MonitorIndex := Screen.MonitorFromWindow(Self.Handle, mdNearest).MonitorNum;

    PluginSetting.PositionX := (PeakMeterOSD.Left - Screen.Monitors[PluginSetting.MonitorIndex].Left
      + PeakMeterOSD.Width div 2) * 100 / Screen.Monitors[PluginSetting.MonitorIndex].Width;

    PluginSetting.PositionY := (PeakMeterOSD.Top - Screen.Monitors[PluginSetting.MonitorIndex].Top +
      PeakMeterOSD.Height div 2) * 100 / Screen.Monitors[PluginSetting.MonitorIndex].Height;

    OptionFrame.UpdateFromSetting;
  end;
end;

procedure TPeakMeterOSD.FormMouseWheelDown(Sender: TObject; Shift: TShiftState; MousePos: TPoint;
  var Handled: Boolean);
begin
  SetVolume(-0.05);
end;

procedure TPeakMeterOSD.FormMouseWheelUp(Sender: TObject; Shift: TShiftState; MousePos: TPoint;
  var Handled: Boolean);
begin
  SetVolume(0.05);
end;

procedure TPeakMeterOSD.mniHideClick(Sender: TObject);
begin
  PluginSetting.ShowPeakMeter := False;
  Self.Hide;
end;

procedure TPeakMeterOSD.mniSettingClick(Sender: TObject);
begin
  OptionFrame.DoShow;
end;

procedure TPeakMeterOSD.mniMovableClick(Sender: TObject);
begin
  PluginSetting.Movable := mniMovable.Checked;
end;

procedure TPeakMeterOSD.mniStayOnTopClick(Sender: TObject);
begin
  PluginSetting.StayOnTop := mniStayOnTop.Checked;
  SetStayOnTopAndPosition(PluginSetting.StayOnTop);
end;

procedure TPeakMeterOSD.pmOSDPopup(Sender: TObject);
begin
  mniMovable.Enabled := not(PluginSetting.InScreenCenter);
end;

procedure TPeakMeterOSD.SetInScreenCenter(const Value: Boolean);
begin
  if Value then
  begin
    Left := Screen.Monitors[PluginSetting.MonitorIndex].Left + Screen.Monitors
      [PluginSetting.MonitorIndex].Width div 2 - (Width div 2);

    Top := Screen.Monitors[PluginSetting.MonitorIndex].Top + Screen.Monitors
      [PluginSetting.MonitorIndex].Height div 2 - (Height div 2);
  end
  else
  begin
    Left := Screen.Monitors[PluginSetting.MonitorIndex].Left +
      Round(Screen.Monitors[PluginSetting.MonitorIndex].Width * PluginSetting.PositionX / 100) -
      Width div 2;

    Top := Screen.Monitors[PluginSetting.MonitorIndex].Top +
      Round(Screen.Monitors[PluginSetting.MonitorIndex].Height * PluginSetting.PositionY / 100) -
      Height div 2;
  end;
end;

procedure TPeakMeterOSD.UpdateLanguage;
begin
  mniSetting.Caption := LangLoadString('PeakMeter\Setting');
  mniMovable.Caption := LangLoadString('PeakMeter\Movable');
  mniStayOnTop.Caption := LangLoadString('PeakMeter\StayOnTop');
  mniHide.Caption := LangLoadString('PeakMeter\Hide');
end;

procedure TPeakMeterOSD.SetMeterTransparent(const Value: Byte);
begin
  LayeredWindow.Alpha := 255 - Round(255 / 100 * Value);
end;

procedure TPeakMeterOSD.SetMovable(const Value: Boolean);
begin
  mniMovable.Checked := Value;
end;

procedure TPeakMeterOSD.SetStayOnTopAndPosition(const Value: Boolean);
begin
  if Value then
    SetWindowPos(Handle, HWND_TOPMOST, Left, Top, Width, Height, SWP_NOMOVE or SWP_NOSIZE)
  else
    SetWindowPos(Handle, HWND_NOTOPMOST, Left, Top, Width, Height, SWP_NOMOVE or SWP_NOSIZE);

  mniStayOnTop.Checked := Value;
end;

end.
